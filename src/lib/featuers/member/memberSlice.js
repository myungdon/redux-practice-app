import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    memberName: '',
    memberAge: 0
}

const memberSlice = createSlice({
    name: 'member',
    initialState,
    reducers: {
        patchMemberName: (state, action) => {
          state.memberName = action.payload
        },
        patchMemberAge: (state, action) => {
            state.memberAge = action.payload
        }
    }
})

export const { patchMemberName, patchMemberAge } = memberSlice.actions
export default memberSlice.reducer