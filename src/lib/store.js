import { configureStore } from '@reduxjs/toolkit'
import memberReducer from "@/lib/featuers/member/memberSlice";

export default configureStore({
    reducer: {
        member: memberReducer
    },
})