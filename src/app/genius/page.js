'use client'

import { useSelector } from "react-redux"; // 값 불러 오기
import Link from "next/link";

export default function Genius() {
    const { memberName } = useSelector(state => state.member)

    return (
        <div>
            <div>{memberName} 님 당신은 천재입니다.</div>
            <Link href="/age">나이 입력하러가기</Link>
        </div>
    )
}