'use client'

import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { patchMemberAge } from "@/lib/featuers/member/memberSlice";
import Link from "next/link";

export default function Age() {
    const [memberAge, setMemberAge] = useState('')
    const { memberName } = useSelector(state => state.member)
    const dispatch = useDispatch()

    const handleSaveAge = () => {
        dispatch(patchMemberAge(Number(memberAge)))
    }

    return (
        <div>
            {memberName} 님 나이를 입력해 주세요.<br />
            <input type="number" value={memberAge} onChange={(e) => setMemberAge(e.target.value)}/>
            <button onClick={handleSaveAge}>저장</button>
            <br/>
            <Link href="/result">결과 보기</Link>
        </div>
    )
}