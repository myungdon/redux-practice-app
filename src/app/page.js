'use client'

import { useState } from "react"; // 입력값을 임시로 저장
import { useDispatch } from "react-redux"; // 중앙 상태 관리에 저장
import { patchMemberName } from "@/lib/featuers/member/memberSlice"; // 관리할 대상
import Link from "next/link"; // 페이지 이동

export default function Home() {
  const [memberName, setMemberName] = useState('')
  const dispatch = useDispatch()

  const handleSaveName = () => {
    dispatch(patchMemberName(memberName))
  }

  return (
    <main>
      <input type="text" value={memberName} onChange={(e) => setMemberName(e.target.value)}/>
      <button onClick={handleSaveName}>저장</button>
      <Link href="/genius">천재 확인</Link>
    </main>
  );
}
// 입력할게 1개면 따로 핸들 만들지 않고 바로 쓰기도 함